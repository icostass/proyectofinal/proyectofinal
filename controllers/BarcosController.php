<?php

namespace app\controllers;

use app\models\Barcos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BarcosController implements the CRUD actions for Barcos model.
 */
class BarcosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Barcos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Barcos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id_barco' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Barcos model.
     * @param int $id_barco Id Barco
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_barco)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_barco),
        ]);
    }

    /**
     * Creates a new Barcos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Barcos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_barco' => $model->id_barco]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Barcos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_barco Id Barco
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_barco)
    {
        $model = $this->findModel($id_barco);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_barco' => $model->id_barco]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Barcos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_barco Id Barco
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_barco)
    {
        $this->findModel($id_barco)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Barcos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_barco Id Barco
     * @return Barcos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_barco)
    {
        if (($model = Barcos::findOne(['id_barco' => $id_barco])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
