<?php

namespace app\controllers;

use app\models\Piratas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;

/**
 * PiratasController implements the CRUD actions for Piratas model.
 */
class PiratasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Piratas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Piratas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id_pirata' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Piratas model.
     * @param int $id_pirata Id Pirata
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_pirata)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_pirata),
        ]);
    }

    /**
     * Creates a new Piratas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Piratas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_pirata' => $model->id_pirata]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Piratas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_pirata Id Pirata
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_pirata)
    {
        $model = $this->findModel($id_pirata);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_pirata' => $model->id_pirata]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Piratas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_pirata Id Pirata
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_pirata)
    {
        $this->findModel($id_pirata)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Piratas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_pirata Id Pirata
     * @return Piratas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_pirata)
    {
        if (($model = Piratas::findOne(['id_pirata' => $id_pirata])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /**
     * Acciones para los datos de los Piratas del Caribe
     */
    
    public function actionEdwardteach(){
     $dataProvider = new ActiveDataProvider([
        'query' => Piratas::find()
            ->select(['nombre', 'mote', 'biografia', 'f_nacimiento', 'f_muerte', 'profesion'])
            ->where(['id_pirata' => 1]),
             ]);
        return $this->render("inforpirata",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionJohnroberts(){
     $dataProvider = new ActiveDataProvider([
        'query' => Piratas::find()
            ->select(['nombre', 'mote', 'biografia', 'f_nacimiento', 'f_muerte', 'profesion'])
            ->where(['id_pirata' => 3]),
             ]);
        return $this->render("inforpirata",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionJohnrackham(){
     $dataProvider = new ActiveDataProvider([
        'query' => Piratas::find()
            ->select(['nombre', 'mote', 'biografia', 'f_nacimiento', 'f_muerte', 'profesion'])
            ->where(['id_pirata' => 4]),
             ]);
        return $this->render("inforpirata",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionCharlesvane(){
     $dataProvider = new ActiveDataProvider([
        'query' => Piratas::find()
            ->select(['nombre', 'mote', 'biografia', 'f_nacimiento', 'f_muerte', 'profesion'])
            ->where(['id_pirata' => 5]),
             ]);
        return $this->render("inforpirata",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionAnneyboney(){
     $dataProvider = new ActiveDataProvider([
        'query' => Piratas::find()
            ->select(['nombre', 'mote', 'biografia', 'f_nacimiento', 'f_muerte', 'profesion'])
            ->where(['id_pirata' => 6]),
             ]);
        return $this->render("inforpirata",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionMaryread(){
     $dataProvider = new ActiveDataProvider([
        'query' => Piratas::find()
            ->select(['nombre', 'mote', 'biografia', 'f_nacimiento', 'f_muerte', 'profesion'])
            ->where(['id_pirata' => 7]),
             ]);
        return $this->render("inforpirata",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
     /**
     * Acciones para los datos de los Piratas del Índico
     */
    
    public function actionWilliamkidd(){
     $dataProvider = new ActiveDataProvider([
        'query' => Piratas::find()
            ->select(['nombre', 'mote', 'biografia', 'f_nacimiento', 'f_muerte', 'profesion'])
            ->where(['id_pirata' => 2]),
             ]);
        return $this->render("inforpirata",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionOlivier(){
     $dataProvider = new ActiveDataProvider([
        'query' => Piratas::find()
            ->select(['nombre', 'mote', 'biografia', 'f_nacimiento', 'f_muerte', 'profesion'])
            ->where(['id_pirata' => 10]),
             ]);
        return $this->render("inforpirata",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    /**
     * Acciones para los datos de los Piratas del Mar de China
     */
    
     public function actionZheng(){
     $dataProvider = new ActiveDataProvider([
        'query' => Piratas::find()
            ->select(['nombre', 'mote', 'biografia', 'f_nacimiento', 'f_muerte', 'profesion'])
            ->where(['id_pirata' => 8]),
             ]);
        return $this->render("inforpirata",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionCheung(){
     $dataProvider = new ActiveDataProvider([
        'query' => Piratas::find()
            ->select(['nombre', 'mote', 'biografia', 'f_nacimiento', 'f_muerte', 'profesion'])
            ->where(['id_pirata' => 9]),
             ]);
        return $this->render("inforpirata",[
            "dataProvider"=>$dataProvider,
        ]);
    }
}
