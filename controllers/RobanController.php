<?php

namespace app\controllers;

use app\models\Roban;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RobanController implements the CRUD actions for Roban model.
 */
class RobanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Roban models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Roban::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_roban' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Roban model.
     * @param int $codigo_roban Codigo Roban
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_roban)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_roban),
        ]);
    }

    /**
     * Creates a new Roban model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Roban();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_roban' => $model->codigo_roban]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Roban model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_roban Codigo Roban
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_roban)
    {
        $model = $this->findModel($codigo_roban);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_roban' => $model->codigo_roban]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Roban model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_roban Codigo Roban
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_roban)
    {
        $this->findModel($codigo_roban)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Roban model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_roban Codigo Roban
     * @return Roban the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_roban)
    {
        if (($model = Roban::findOne(['codigo_roban' => $codigo_roban])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
