<?php

namespace app\controllers;

use app\models\Zonas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ZonasController implements the CRUD actions for Zonas model.
 */
class ZonasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Zonas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Zonas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id_zona' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zonas model.
     * @param int $id_zona Id Zona
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_zona)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_zona),
        ]);
    }

    /**
     * Creates a new Zonas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Zonas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_zona' => $model->id_zona]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Zonas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_zona Id Zona
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_zona)
    {
        $model = $this->findModel($id_zona);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_zona' => $model->id_zona]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Zonas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_zona Id Zona
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_zona)
    {
        $this->findModel($id_zona)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Zonas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_zona Id Zona
     * @return Zonas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_zona)
    {
        if (($model = Zonas::findOne(['id_zona' => $id_zona])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
