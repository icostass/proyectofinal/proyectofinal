<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Armas".
 *
 * @property int $codigo_armas
 * @property int $id_piratas
 * @property string $armas
 */
class Armas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Armas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_piratas', 'armas'], 'required'],
            [['id_piratas'], 'integer'],
            [['armas'],'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ.,;\s]+$/",'message' => 'Sólo letras.', 'max' => 50],
            [['id_piratas', 'armas'], 'unique', 'targetAttribute' => ['id_piratas', 'armas']],
            [['id_piratas'], 'exist', 'skipOnError' => true, 'targetClass' => Piratas::class, 'targetAttribute' => ['id_piratas' => 'id_pirata']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_armas' => 'Codigo Armas',
            'id_piratas' => 'Id Piratas',
            'armas' => 'Armas',
        ];
    }
    
}
