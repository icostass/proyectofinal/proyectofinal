<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "barcos".
 *
 * @property string|null $nombre
 * @property string|null $tipo
 * @property int|null $numero_canones
 * @property string|null $capitan
 * @property int $id_barco
 *
 * @property Piratas[] $piratas
 */
class Barcos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'barcos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_canones'], 'integer'],
            [['nombre', 'tipo', 'capitan'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'tipo' => 'Tipo',
            'numero_canones' => 'Numero Canones',
            'capitan' => 'Capitan',
            'id_barco' => 'Id Barco',
        ];
    }

    /**
     * Gets query for [[Piratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPiratas()
    {
        return $this->hasMany(Piratas::class, ['id_barcos' => 'id_barco']);
    }
}
