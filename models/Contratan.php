<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contratan".
 *
 * @property int $codigo_contratan
 * @property string|null $fecha_contrato
 * @property int $id_paises
 * @property int $id_piratas
 *
 * @property Paises $paises
 * @property Piratas $piratas
 */
class Contratan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contratan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_paises', 'id_piratas'], 'required'],
            [['id_paises', 'id_piratas'], 'integer'],
            [['fecha_contrato'], 'string', 'max' => 15],
            [['id_paises', 'id_piratas'], 'unique', 'targetAttribute' => ['id_paises', 'id_piratas']],
            [['id_paises'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::class, 'targetAttribute' => ['id_paises' => 'id']],
            [['id_piratas'], 'exist', 'skipOnError' => true, 'targetClass' => Piratas::class, 'targetAttribute' => ['id_piratas' => 'id_pirata']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_contratan' => 'Codigo Contratan',
            'fecha_contrato' => 'Fecha Contrato',
            'id_paises' => 'Id Paises',
            'id_piratas' => 'Id Piratas',
        ];
    }

    /**
     * Gets query for [[Paises]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaises()
    {
        return $this->hasOne(Paises::class, ['id' => 'id_paises']);
    }

    /**
     * Gets query for [[Piratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPiratas()
    {
        return $this->hasOne(Piratas::class, ['id_pirata' => 'id_piratas']);
    }
}
