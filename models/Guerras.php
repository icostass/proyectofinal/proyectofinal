<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guerras".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $duracion
 *
 * @property Participan[] $participans
 */
class Guerras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guerras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'duracion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'duracion' => 'Duracion',
        ];
    }

    /**
     * Gets query for [[Participans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipans()
    {
        return $this->hasMany(Participan::class, ['id_guerras' => 'id']);
    }
}
