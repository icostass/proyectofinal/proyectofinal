<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paises".
 *
 * @property string|null $nombre
 * @property string|null $gobernante
 * @property int $id
 *
 * @property Contratan[] $contratans
 * @property Participan[] $participans
 * @property Piratas[] $piratas
 * @property Piratas[] $piratas0
 * @property Zonas[] $zonas
 */
class Paises extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paises';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'gobernante'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'gobernante' => 'Gobernante',
            'id' => 'ID',
        ];
    }

    /**
     * Gets query for [[Contratans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratans()
    {
        return $this->hasMany(Contratan::class, ['id_paises' => 'id']);
    }

    /**
     * Gets query for [[Participans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipans()
    {
        return $this->hasMany(Participan::class, ['id_paises' => 'id']);
    }

    /**
     * Gets query for [[Piratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPiratas()
    {
        return $this->hasMany(Piratas::class, ['id_paises' => 'id']);
    }

    /**
     * Gets query for [[Piratas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPiratas0()
    {
        return $this->hasMany(Piratas::class, ['id_pirata' => 'id_piratas'])->viaTable('contratan', ['id_paises' => 'id']);
    }

    /**
     * Gets query for [[Zonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZonas()
    {
        return $this->hasMany(Zonas::class, ['id_pais' => 'id']);
    }
}
