<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "participan".
 *
 * @property int $id
 * @property int|null $id_paises
 * @property int|null $id_guerras
 *
 * @property Guerras $guerras
 * @property Paises $paises
 */
class Participan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'participan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_paises', 'id_guerras'], 'integer'],
            [['id_guerras'], 'exist', 'skipOnError' => true, 'targetClass' => Guerras::class, 'targetAttribute' => ['id_guerras' => 'id']],
            [['id_paises'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::class, 'targetAttribute' => ['id_paises' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paises' => 'Id Paises',
            'id_guerras' => 'Id Guerras',
        ];
    }

    /**
     * Gets query for [[Guerras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGuerras()
    {
        return $this->hasOne(Guerras::class, ['id' => 'id_guerras']);
    }

    /**
     * Gets query for [[Paises]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaises()
    {
        return $this->hasOne(Paises::class, ['id' => 'id_paises']);
    }
}
