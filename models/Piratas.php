<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "piratas".
 *
 * @property string|null $nombre
 * @property string|null $mote
 * @property string|null $f_nacimiento
 * @property string|null $f_muerte
 * @property string|null $biografia
 * @property string|null $profesion
 * @property int $id_pirata
 * @property int|null $id_barcos
 * @property int|null $id_paises
 *
 * @property Armas[] $armas
 * @property Barcos $barcos
 * @property Contratan[] $contratans
 * @property Paises $paises
 * @property Paises[] $paises0
 * @property Roban[] $robans
 * @property Zonas[] $zonas
 */
class Piratas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piratas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['biografia'], 'string'],
            [['id_barcos', 'id_paises'], 'integer'],
            [['nombre', 'mote', 'profesion'], 'string', 'max' => 100],
            [['f_nacimiento', 'f_muerte'], 'string', 'max' => 15],
            [['id_barcos'], 'exist', 'skipOnError' => true, 'targetClass' => Barcos::class, 'targetAttribute' => ['id_barcos' => 'id_barco']],
            [['id_paises'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::class, 'targetAttribute' => ['id_paises' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'mote' => 'Mote',
            'f_nacimiento' => 'F Nacimiento',
            'f_muerte' => 'F Muerte',
            'biografia' => 'Biografia',
            'profesion' => 'Profesion',
            'id_pirata' => 'Id Pirata',
            'id_barcos' => 'Id Barcos',
            'id_paises' => 'Id Paises',
        ];
    }

    /**
     * Gets query for [[Armas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArmas()
    {
        return $this->hasMany(Armas::class, ['id_piratas' => 'id_pirata']);
    }

    /**
     * Gets query for [[Barcos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBarcos()
    {
        return $this->hasOne(Barcos::class, ['id_barco' => 'id_barcos']);
    }

    /**
     * Gets query for [[Contratans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratans()
    {
        return $this->hasMany(Contratan::class, ['id_piratas' => 'id_pirata']);
    }

    /**
     * Gets query for [[Paises]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaises()
    {
        return $this->hasOne(Paises::class, ['id' => 'id_paises']);
    }

    /**
     * Gets query for [[Paises0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaises0()
    {
        return $this->hasMany(Paises::class, ['id' => 'id_paises'])->viaTable('contratan', ['id_piratas' => 'id_pirata']);
    }

    /**
     * Gets query for [[Robans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRobans()
    {
        return $this->hasMany(Roban::class, ['id_piratas' => 'id_pirata']);
    }

    /**
     * Gets query for [[Zonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZonas()
    {
        return $this->hasMany(Zonas::class, ['id_zona' => 'id_zonas'])->viaTable('roban', ['id_piratas' => 'id_pirata']);
    }
}
