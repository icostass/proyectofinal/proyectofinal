<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "roban".
 *
 * @property int $codigo_roban
 * @property int|null $id_piratas
 * @property int|null $id_zonas
 *
 * @property Piratas $piratas
 * @property Zonas $zonas
 */
class Roban extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'roban';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_piratas', 'id_zonas'], 'integer'],
            [['id_piratas', 'id_zonas'], 'unique', 'targetAttribute' => ['id_piratas', 'id_zonas']],
            [['id_zonas'], 'exist', 'skipOnError' => true, 'targetClass' => Zonas::class, 'targetAttribute' => ['id_zonas' => 'id_zona']],
            [['id_piratas'], 'exist', 'skipOnError' => true, 'targetClass' => Piratas::class, 'targetAttribute' => ['id_piratas' => 'id_pirata']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_roban' => 'Codigo Roban',
            'id_piratas' => 'Id Piratas',
            'id_zonas' => 'Id Zonas',
        ];
    }

    /**
     * Gets query for [[Piratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPiratas()
    {
        return $this->hasOne(Piratas::class, ['id_pirata' => 'id_piratas']);
    }

    /**
     * Gets query for [[Zonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZonas()
    {
        return $this->hasOne(Zonas::class, ['id_zona' => 'id_zonas']);
    }
}
