<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "zonas".
 *
 * @property string|null $nombre
 * @property string $longitud
 * @property string $latitud
 * @property string|null $fecha_dominio
 * @property int $id_pais
 * @property int $id_zona
 *
 * @property Paises $pais
 * @property Piratas[] $piratas
 * @property Roban[] $robans
 */
class Zonas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zonas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['longitud', 'latitud', 'id_pais', 'id_zona'], 'required'],
            [['id_pais', 'id_zona'], 'integer'],
            [['nombre', 'longitud', 'latitud'], 'string', 'max' => 30],
            [['fecha_dominio'], 'string', 'max' => 50],
            [['id_zona'], 'unique'],
            [['id_pais'], 'exist', 'skipOnError' => true, 'targetClass' => Paises::class, 'targetAttribute' => ['id_pais' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'longitud' => 'Longitud',
            'latitud' => 'Latitud',
            'fecha_dominio' => 'Fecha Dominio',
            'id_pais' => 'Id Pais',
            'id_zona' => 'Id Zona',
        ];
    }

    /**
     * Gets query for [[Pais]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPais()
    {
        return $this->hasOne(Paises::class, ['id' => 'id_pais']);
    }

    /**
     * Gets query for [[Piratas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPiratas()
    {
        return $this->hasMany(Piratas::class, ['id_pirata' => 'id_piratas'])->viaTable('roban', ['id_zonas' => 'id_zona']);
    }

    /**
     * Gets query for [[Robans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRobans()
    {
        return $this->hasMany(Roban::class, ['id_zonas' => 'id_zona']);
    }
}
