<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Armas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="armas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_piratas')->textInput() ?>

    <?= $form->field($model, 'armas')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
