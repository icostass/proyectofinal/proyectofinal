<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\bootstrap4\Button;

$this->title = "Armas del pirata";

 ?>
<!-- Maquetación de las tarjetas pertenecientes al listview -->
<div class="col-sm-12" >
    <div class="card alturaminima">
        <div class="card-body tarjeta">
            <h3 class="tituloObra" style="text-align: center;"><?= Html::img('@web/img/' . $model->armas . '.jpg', ['alt' => 'My logo']) ?></h3>
            <h2 style="text-align: center; text-transform: uppercase;"> <?= $model->armas ?></h2>
        </div>
    </div>
</div>





