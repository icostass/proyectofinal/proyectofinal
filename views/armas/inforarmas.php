<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;

$titulo="Armas del pirata"
?>

<!-- Botón desplegable 'Volver' de la vista de armas con sus respectivos botones que permiten volver a cualquiera de los mapas deseados -->
<div class="btn-group btn-mapamundi " role="group">
     <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle btn-dark btn-armas" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Volver
     </button>
        <div class="dropdown-menu btn-armas" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Mar Caribe', ['site/caribe'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
              <?= Html::a('Océano Índico', ['site/indico'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
              <?= Html::a('Mar de China', ['site/china'],   ['class'=>'btn btn-dark btn-dentrodesple'])?>
            </p>
        </div>
</div>

<!-- Botón desplegable 'Pirata'  con sus botones que permiten elegir el pirata deseado para ver sus armas -->
<div class="btn-group btn-desple " role="group">
     <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle btn-dark btn-armas" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Pirata
     </button>
        <div class="dropdown-menu btn-armas" aria-labelledby="btnGroupDrop1">
            <p>
                <?= Html::a('Edward Teach', ['armas/edwardteach'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
                <?= Html::a('John Roberts', ['armas/johnroberts'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
                <?= Html::a('John Rackham', ['armas/johnrackham'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
                <?= Html::a('Charles Vane', ['armas/charlesvane'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
                <?= Html::a('Anney Boney', ['armas/anneyboney'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
                <?= Html::a('Mary Read', ['armas/maryread'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
                <?= Html::a('William Kidd', ['armas/williamkidd'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
                <?= Html::a('Olivier', ['armas/olivier'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
                <?= Html::a('Zheng Shi', ['armas/zhengshi'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
                <?= Html::a('Cheung', ['armas/cheung'], ['class'=>'btn btn-dark btn-dentrodesple'])?>
            </p>
        </div>
</div>

<!-- Título de la vista -->
<h2 style=" margin-top: 100px; margin-left: 65%; font-size: 50px;"> <?= $titulo ?></h2>
 
<!-- ListView de la vista de las armas por tarjetas -->
<div style="margin-top: 30px; position: absolute; z-index: 1;">
    <div class="row">
    <div class=" body-content" style="color:#4A4A4A; float: left;">
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_inforarmas',
                'layout' => " \n {items} \n\n{pager}",
                'itemOptions' => [
                    'style' => 'float: left; justify-content: center; margin-top: 10px; ',
                ],
            ]);
            ?>

    </div>
    </div>
</div>

