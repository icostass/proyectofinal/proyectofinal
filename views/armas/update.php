<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Armas $model */

$this->title = 'Update Armas: ' . $model->codigo_armas;
$this->params['breadcrumbs'][] = ['label' => 'Armas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_armas, 'url' => ['view', 'codigo_armas' => $model->codigo_armas]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="armas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
