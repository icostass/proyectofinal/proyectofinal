<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Barcos $model */

$this->title = 'Update Barcos: ' . $model->id_barco;
$this->params['breadcrumbs'][] = ['label' => 'Barcos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_barco, 'url' => ['view', 'id_barco' => $model->id_barco]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="barcos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
