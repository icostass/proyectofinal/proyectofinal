<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Barcos $model */

$this->title = $model->id_barco;
$this->params['breadcrumbs'][] = ['label' => 'Barcos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="barcos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_barco' => $model->id_barco], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_barco' => $model->id_barco], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'tipo',
            'numero_canones',
            'capitan',
            'id_barco',
        ],
    ]) ?>

</div>
