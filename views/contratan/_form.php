<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Contratan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="contratan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fecha_contrato')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_paises')->textInput() ?>

    <?= $form->field($model, 'id_piratas')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
