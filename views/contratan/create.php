<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Contratan $model */

$this->title = 'Create Contratan';
$this->params['breadcrumbs'][] = ['label' => 'Contratans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contratan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
