<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Contratan $model */

$this->title = 'Update Contratan: ' . $model->codigo_contratan;
$this->params['breadcrumbs'][] = ['label' => 'Contratans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_contratan, 'url' => ['view', 'codigo_contratan' => $model->codigo_contratan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contratan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
