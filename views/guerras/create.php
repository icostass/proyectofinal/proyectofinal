<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Guerras $model */

$this->title = 'Create Guerras';
$this->params['breadcrumbs'][] = ['label' => 'Guerras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guerras-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
