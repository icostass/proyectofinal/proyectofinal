<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Guerras $model */

$this->title = 'Update Guerras: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Guerras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="guerras-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
