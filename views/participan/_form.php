<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Participan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="participan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_paises')->textInput() ?>

    <?= $form->field($model, 'id_guerras')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
