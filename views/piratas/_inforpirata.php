<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\bootstrap4\Button;

$this->title = "Información del pirata";
$mote = "Mote";
 ?>

<!-- Maquetación de cómo se muestra la información de los piratas -->
<div style="float: left; margin-right: 10px;">
    <h2><u><?= $model->nombre?></u></h2> 
</div>
<div style="float: left; margin-right: 10px;">   
    <h2 style="font-size: 30px;"> <i><?= $model->mote?></i> </h2> 
</div>
<div>
    <h2>(<?= $model->f_nacimiento?> / <?= $model->f_muerte?>)</h2> 
</div>
<div style="margin-top: 35px;">
    <h2 style="font-size: 30px;"> <?= $model->biografia?></h2>
</div>
<div style="text-align: center; margin-top: 3%;">
<?= Html::img('@web/img/' . $model->nombre . '.jpg', ['alt' => 'My logo']) ?>
</div>
