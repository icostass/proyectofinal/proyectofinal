<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Piratas $model */

$this->title = 'Create Piratas';
$this->params['breadcrumbs'][] = ['label' => 'Piratas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="piratas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
