<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;

$titulo="Información del pirata"
?>

<!-- Botón para volver a la página anterior -->
<?=Html::a('Volver', Yii::$app->request->referrer ?: Yii::$app->homeUrl, ['class' => 'btn btn-mapamundi btn-dark']); ?>

<!-- Maquetación del ListView para la información de los piratas -->
<div style="margin-top: 100px; margin-right: 50px;  position: absolute; z-index: 1;">
    <h2 style="text-align: center; font-size: 50px;"> <?= $titulo ?></h2>

    
    <div class="row" style="margin-top: 50px;"> 
         <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_inforpirata',
                'summary' =>''
             ]);
            ?>
    </div>
</div>

