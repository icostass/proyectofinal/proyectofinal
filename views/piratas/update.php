<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Piratas $model */

$this->title = 'Update Piratas: ' . $model->id_pirata;
$this->params['breadcrumbs'][] = ['label' => 'Piratas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pirata, 'url' => ['view', 'id_pirata' => $model->id_pirata]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="piratas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
