<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Piratas $model */

$this->title = $model->id_pirata;
$this->params['breadcrumbs'][] = ['label' => 'Piratas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="piratas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_pirata' => $model->id_pirata], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_pirata' => $model->id_pirata], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'mote',
            'biografia:ntext',
            'f_nacimiento',
            'f_muerte',
            'profesion',
            'id_pirata',
            'id_barcos',
            'id_paises',
        ],
    ]) ?>

</div>
