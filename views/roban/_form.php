<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Roban $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="roban-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_piratas')->textInput() ?>

    <?= $form->field($model, 'id_zonas')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
