<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Roban $model */

$this->title = 'Create Roban';
$this->params['breadcrumbs'][] = ['label' => 'Robans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="roban-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
