<?php

use app\models\Roban;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Robans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="roban-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Roban', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_roban',
            'id_piratas',
            'id_zonas',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Roban $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_roban' => $model->codigo_roban]);
                 }
            ],
        ],
    ]); ?>


</div>
