<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Roban $model */

$this->title = 'Update Roban: ' . $model->codigo_roban;
$this->params['breadcrumbs'][] = ['label' => 'Robans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_roban, 'url' => ['view', 'codigo_roban' => $model->codigo_roban]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="roban-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
