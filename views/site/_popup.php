<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

?>
<!DOCTYPE html>

<!-- Maquetación del Pop up -->
<body>
    <header>
        <h2 style="text-align: center;"> 
            Bienvenido a bordo polizón
        </h2>
    </header>
    <main class="popuptext">
        <p class="fuentepd"> <b>
            En esta web podrás obtener información de los piratas más temidos de los siete mares. <br>
            Para ver la información de los corsarios del mar, elige uno de los mares disponibles en el siguiente mapa. <br>
            Una vez elegido se podrá indagar sobre las historias de estos piratas e información la mar de interesante.
            </b></p>
    </main>
</body>