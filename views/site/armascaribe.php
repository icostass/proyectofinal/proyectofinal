<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\bootstrap4\Button;
use yii\bootstrap4\Dropdown;

/** @var yii\web\View $this */


$this->title = 'Mar Caribe';
$this->registerJsFile('@web/js/sitejs.js');

?>

    <!--Botón para volver al mapamundi-->
<?= Html::a($text = "Mapamundi", ['site/index'], ['class' => 'btn btn-mapamundi btn-dark']) ?>

<div class="containercaribe" style="z-index: 1;">  
    
<h2 style="text-align: center;"> Bienvenido al Mar Caribe polizón</h2>


<!-- Menú lateral -->
<p class="textlat"> Selecciona una de las opciones: </p>
    <div class="dot"></div>
    
      <?= Html::a('Mapa Caribe', ['site/caribe'], ['class'=>'btn btn-dark btn-mapassecun'])?>
</div>    
    
    <!-- Opción del desplegable para ver las armas -->
   <div class="dot2"></div> 
   
        <?= Html::a('Armas', ['site/armascaribe'], ['class'=>'btn btn-dark btn-datos'])?>
   <!-- Botones para ver las armas -->
    <div class="selarmas" style="position: absolute;">

        
        
        <h2 style="font-size: 40px;"> Selecciona un pirata </h2>
        
                <?= Html::a('Edward Teach', ['armas/edwardteach'], ['class'=>'btn btn-dark btn-armas'])?>
                <?= Html::a('John Roberts', ['armas/johnroberts'], ['class'=>'btn btn-dark btn-armas'])?>
                <?= Html::a('John Rackham', ['armas/johnrackham'], ['class'=>'btn btn-dark btn-armas'])?>
                <?= Html::a('Charles Vane', ['armas/charlesvane'], ['class'=>'btn btn-dark btn-armas'])?>
                <?= Html::a('Anney Boney', ['armas/anneyboney'], ['class'=>'btn btn-dark btn-armas'])?>
                <?= Html::a('Mary Read', ['armas/maryread'], ['class'=>'btn btn-dark btn-armas'])?>

  
        
    </div>
     
 





   
