<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\bootstrap4\Button;
use yii\web\JsExpression;
/** @var yii\web\View $this */


$this->title = 'Mar de China';

?>

<?= Html::a($text = "Mapamundi", ['site/index'], ['class' => 'btn btn-mapamundi btn-dark']) ?>

<div class="containerchina" style="z-index: 1;">    
<h2 style="text-align: center;"> Bienvenido al Mar de China polizón</h2>

<!-- Menú lateral -->
<p class="textlat"> Selecciona una de las opciones: </p>
    <div class="dot"></div>
    <?= Html::a('Mapa China', ['site/china'], ['class'=>'btn btn-dark btn-mapassecun'])?>
  </div>  

<!-- Opción del desplegable para ver las armas -->
   <div class="dot2"></div> 
   
<?= Html::a('Armas', ['site/armaschina'], ['class'=>'btn btn-dark btn-datos'])?>
   
   <!-- Botones para ver las armas --> 
    <div class="selarmas" style="position: absolute;">
     
            <h2 style="font-size: 40px;"> Selecciona un pirata </h2>
               
                <?= Html::a('Zheng Shi', ['armas/zhengshi'], ['class'=>'btn btn-dark btn-armas'])?>
                <?= Html::a('Cheung Po Tsai', ['armas/cheung'], ['class'=>'btn btn-dark btn-armas'])?>
 
        
    </div>

