<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\bootstrap4\Button;

/** @var yii\web\View $this */


$this->title = 'Océano índico';
$this->registerJsFile('@web/js/sitejs.js');
?>
<?= Html::a($text = "Mapamundi", ['site/index'], ['class' => 'btn btn-mapamundi btn-dark']) ?>

<div class="containerindico" style="z-index: 1;">    
<h2 class="textind"> Bienvenido al Océano Índico polizón</h2>

<!-- Menú lateral -->
<p class="textlat"> Selecciona una de las opciones: </p>
<div class="dot"></div>
    <?= Html::a('Mapa Índico', ['site/indico'], ['class'=>'btn btn-dark btn-mapassecun'])?>
 </div>   
<!-- Opción del desplegable para ver las armas -->
     <div class="dot2"></div> 
     
      <?= Html::a('Armas', ['site/armasindico'], ['class'=>'btn btn-dark btn-datos'])?>
    <!-- Botones para ver las armas -->
    <div class="selarmas" style="position: absolute;">
       
            <h2 style="font-size: 40px;"> Selecciona un pirata </h2>
            
                <?= Html::a('William Kidd', ['armas/williamkidd'], ['class'=>'btn btn-dark btn-armas'])?>
                <?= Html::a('Olivier Levasseur', ['armas/olivier'], ['class'=>'btn btn-dark btn-armas'])?>
      
    </div>
    
         
 




   
