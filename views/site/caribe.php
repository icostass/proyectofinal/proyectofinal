<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\bootstrap4\Button;
use yii\bootstrap4\Dropdown;

/** @var yii\web\View $this */


$this->title = 'Mar Caribe';
$this->registerJsFile('@web/js/sitejs.js');

?>

    <!--Botón para volver al mapamundi-->
<?= Html::a($text = "Mapamundi", ['site/index'], ['class' => 'btn btn-mapamundi btn-dark']) ?>

<div class="containercaribe" style="z-index: 1;">  
    
<h2 style="text-align: center; font-size: 50px;"> Bienvenido al Mar Caribe, polizón</h2>


<!-- Menú lateral -->
<p class="textlat"> Selecciona una de las opciones: </p>
    <div class="dot"></div>
    
        <?= Html::a('Mapa Caribe', ['site/caribe'], ['class'=>'btn btn-dark btn-mapassecun'])?>
    
       <!-- Mapa y botones de piratas -->
    <div class="mapa">
        <?php   
        echo Html::img('@web/img/Caribe.png', ['class' => 'img-fluid', 'alt' => 'Mar Caribe']);
        echo Html::a(Html::img('@web/img/calavera.png', ['alt' => 'Imagen del botón', 'class' => 'imagen-pequena',]),['piratas/edwardteach'], ['class' => ' btn-gen btn-et', 'title' => 'Edward Teach']);
        echo Html::a(Html::img('@web/img/calavera.png', ['alt' => 'Imagen del botón', 'class' => 'imagen-pequena',]),['piratas/johnroberts'], ['class' => ' btn-gen btn-jr', 'title' => 'John Roberts']);
        echo Html::a(Html::img('@web/img/calavera.png', ['alt' => 'Imagen del botón', 'class' => 'imagen-pequena',]),['piratas/johnrackham'], ['class' => ' btn-gen btn-cj', 'title' => 'John Rackham']);
        echo Html::a(Html::img('@web/img/calavera.png', ['alt' => 'Imagen del botón', 'class' => 'imagen-pequena',]),['piratas/charlesvane'], ['class' => ' btn-gen btn-cv', 'title' => 'Charles Vane']);
        echo Html::a(Html::img('@web/img/calavera.png', ['alt' => 'Imagen del botón', 'class' => 'imagen-pequena',]),['piratas/anneyboney'], ['class' => ' btn-gen btn-ab', 'title' => 'Anney Bonney']);
        echo Html::a(Html::img('@web/img/calavera.png', ['alt' => 'Imagen del botón', 'class' => 'imagen-pequena',]),['piratas/maryread'], ['class' => ' btn-gen btn-mr', 'title' => 'Mary Read']);

        ?>
    </div>
 </div>    
    <!-- Opción del desplegable para ver las armas -->
   <div class="dot2"></div> 
    <?= Html::a('Armas', ['site/armascaribe'], ['class'=>'btn btn-dark btn-datos'])?>
   

     
 





   
