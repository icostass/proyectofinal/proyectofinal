<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\bootstrap4\Button;
use yii\web\JsExpression;
/** @var yii\web\View $this */


$this->title = 'Mar de China';

?>

<?= Html::a($text = "Mapamundi", ['site/index'], ['class' => 'btn btn-mapamundi btn-dark']) ?>

<div class="containerchina" style="z-index: 1;">    
<h2 style="text-align: center; font-size: 50px;"> Bienvenido al Mar de China, polizón</h2>

<!-- Menú lateral -->
<p class="textlat"> Selecciona una de las opciones: </p>
 <?= Html::a('Mapa China', ['site/china'], ['class'=>'btn btn-dark btn-mapassecun'])?>
    <div class="dot"></div>
     <!-- Mapa y botones de piratas -->
    <div class="mapa">
        <?php
       
        echo Html::img('@web/img/china.png', ['alt' => 'Mar de China']); 
        echo Html::a(Html::img('@web/img/calavera.png', ['alt' => 'Imagen del botón', 'class' => 'imagen-pequena',]),['piratas/zheng'], ['class' => ' btn-gen btn-zs', 'title' => 'Zheng Shi']);
        echo Html::a(Html::img('@web/img/calavera.png', ['alt' => 'Imagen del botón', 'class' => 'imagen-pequena',]),['piratas/cheung'], ['class' => ' btn-gen btn-cpt', 'title' => 'Cheung Po Tsai']);
       
        ?>
    </div>
  </div>  

<!-- Opción del desplegable para ver las armas -->
   <div class="dot2"></div> 
     <?= Html::a('Armas', ['site/armaschina'], ['class'=>'btn btn-dark btn-datos'])?>

