<?php

use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Button;
use dosamigos\chartjs\ChartJs;
use yii\db\Query;

// Consulta para obtener el número de guerras libradas.
$query = new Query();
$guerras = $query->select(['p.nombre AS pais', 'COUNT(pa.id_guerras) as num_guerras'])
        ->from('paises p')
        ->leftJoin('participan pa', 'p.id = pa.id_paises')
        ->groupBy('p.id')
        ->all();

$pais = [];
$nguerras = [];

foreach ($guerras as $guerra) {
    $pais[] = $guerra['pais'];
    $nguerras[] = $guerra['num_guerras'];
}

//Consulta para obtener el número de piratas contratados.
$query = new Query();
$contratos = $query->select(['p.nombre AS pais', 'COUNT(co.id_piratas) as num_piratas'])
        ->from('paises p')
        ->leftJoin('contratan co', 'p.id = co.id_paises')
        ->groupBy('p.id')
        ->all();

$contrato = [];
$npiratas = [];

foreach ($contratos as $contrat) {
    $contrato[] = $contrat['pais'];
    $npiratas[] = $contrat['num_piratas'];
}

?>

<!-- Botón para volver al mapamundi -->
<?= Html::a($text = "Mapamundi", ['site/index'], ['class' => 'btn btn-mapamundi btn-dark']) ?>

<!-- Título de la página -->
<h1 style="margin-top: 50px; margin-left: 68.5%;">Información en gráficos </h1>


<!-- Menú lateral -->
<p class="textlat"> Selecciona una de las opciones: </p>  

<!-- Punto lateral -->
<div class="dot"></div>

<!-- Opción de los países en el menú lateral como un desplegable -->
<?php
echo Button::widget([
    'label' => 'Países',
    'options' => [
        'id' => 'btnToggle',
        'class' => 'btn btn-dark active btn-mapassecun ',
        'data-toggle' => 'collapse',
        'data-target' => '#paises',
        'aria-expanded' => 'true',
        'aria-controls' => 'paises'
    ],
]);
?>

<?php
echo Html::beginTag('paises', ['class' => 'collapse show', 'id' => 'paises']);
?>

<div style="margin-left: 12.5%;">    

    <!-- Gráfico de guerras -->     
    <div style="height: 500px; width: 500px; margin-top: 50px; margin-left: 175px; position: absolute; z-index: 1;">

        <h2> Nº de guerras por país  </h2>

        <?=
        ChartJs::widget([
            'type' => 'line',
            'options' => [
                'height' => 50,
                'width' => 50
            ],
            'data' => [
                'labels' => $pais,
                'datasets' => [
                    [
                        'label' => $pais,
                        'backgroundColor' => "rgba(255, 20, 36, 0.5)",
                        'borderColor' => "rgba(179,181,198,1)",
                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                        'pointBorderColor' => "#fff",
                        'pointHoverBackgroundColor' => "#fff",
                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                        'data' => $nguerras
                    ]
                ]
            ]
        ]);
        ?>
    </div>

    <!-- Gráfico de contratos -->
    <div style="height: 500px; width: 500px; margin-top: 50px; margin-left: 1000px; position: absolute; z-index: 1;">
        <h2> Nº de piratas contratados por país </h2>
        <?=
        ChartJs::widget([
            'type' => 'bar',
            'options' => [
                'height' => 50,
                'width' => 50
            ],
            'data' => [
                'labels' => $contrato,
                'datasets' => [
                    [
                        'label' => $contrato,
                        'backgroundColor' => "rgba(255, 20, 36, 0.5)",
                        'borderColor' => "rgba(179,181,198,1)",
                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                        'pointBorderColor' => "#fff",
                        'pointHoverBackgroundColor' => "#fff",
                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                        'data' => $npiratas
                    ]
                ]
            ]
        ]);
        ?>
    </div>   
</div>
<?php
echo Html::endTag('paises');
?>

<!-- Punto lateral -->
<div class="dot2"></div> 

<!-- Botón de acceso a la información gráfica sobre los barcos -->
<?= Html::a('Barcos', ['site/graficosbarcos'], ['class'=>'btn btn-dark btn-datos'])?>

