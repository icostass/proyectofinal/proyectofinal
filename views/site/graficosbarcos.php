<?php

use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Button;
use dosamigos\chartjs\ChartJs;
use yii\db\Query;


//Consulta para los barcos
$query = new Query();
$barcos = $query->select(['b.nombre', 'b.numero_canones'])
        ->from('barcos b')
        ->all();

$nombrebarco = [];
$ncanones = [];

foreach ($barcos as $barco) {
    $nombrebarco[] = $barco['nombre'];
    $ncanones[] = $barco['numero_canones'];
}
?>

<!-- Botón para volver al mapamundi -->
<?= Html::a($text = "Mapamundi", ['site/index'], ['class' => 'btn btn-mapamundi btn-dark']) ?>

<!-- Título de la página -->
<h1 style="margin-top: 50px; margin-left: 68.5%;">Información en gráficos </h1>

<!<!-- Menú lateral -->
<p class="textlat"> Selecciona una de las opciones: </p>  

<!-- Punto lateral -->
<div class="dot"></div>

<!-- Botón para acceder a la información en gráficos de los paises -->
<?= Html::a('Países', ['site/graficos'], ['class'=>'btn btn-dark btn-mapassecun'])?>

<!-- Punto lateral -->
<div class="dot2"></div> 

<!-- Opción de los países en el menú lateral como un desplegable -->
<?php
echo Button::widget([
    'label' => 'Barcos',
    'options' => [
        'id' => 'btnToggle',
        'class' => 'btn btn-dark active btn-datos ',
        'data-toggle' => 'collapse',
        'data-target' => '#barcos',
        'aria-expanded' => 'true',
        'aria-controls' => 'barcos'
    ],
]);
?>

<!-- Gráfico de los barcos -->
<div class="selarmas" style="position: absolute;">
    <?php
    echo Html::beginTag('barcos', ['class' => 'collapse show', 'id' => 'barcos']);
    ?>

    <div style="height: 600px; width: 600px; margin-top: 15px; margin-left: 180px; position: absolute; z-index: 1;">

        <h2> Nº de cañones por barco</h2>

        <?=
        ChartJs::widget([
            'type' => 'bar',
            'options' => [
                'height' => 400,
                'width' => 400
            ],
            'data' => [
                'labels' => $nombrebarco,
                'datasets' => [
                    [
                        'label' => $nombrebarco,
                        'backgroundColor' => "rgba(255, 20, 36, 0.5)",
                        'borderColor' => "rgba(179,181,198,1)",
                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                        'pointBorderColor' => "#fff",
                        'pointHoverBackgroundColor' => "#fff",
                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                        'data' => $ncanones
                    ]
                ]
            ]
        ]);
        ?>
    </div>

    <?php
    echo Html::endTag('barcos');
    ?>

</div>

