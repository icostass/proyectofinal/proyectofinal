<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
/** @var yii\web\View $this */

$this->registerJs(
    "
    $(document).ready(function(){
        $('#popup-modal').modal('show');
    });
    ",
    \yii\web\View::POS_READY
);

$this->title = 'Mapamundi';
$this->registerJsFile('@web/js/sitejs.js');

?>
<!-- Botón para ir a la pestaña de gráficos -->
<?= Html::a($text = "Gráficos", ['site/graficos'], ['class' => 'btn btn-graficos btn-dark']) ?>


<!-- Código del popup-->
<div id="popup-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <?= $this->renderAjax('_popup') ?>
            </div>
        </div>
    </div>
</div>

<!-- Código del mapa con sus botones -->
<div class="container" style="z-index: 1;">    
    
    <div class="mapa">
            <?= Html::img('@web/img/Mapamundi.png', ['alt' => 'Mapamundi']) ?>
        
            <?= Html::a($text = "Mar Caribe", ['site/caribe'], ['class' => 'btn-gen btn-caribe']) ?>
            <?= Html::a($text = "Océano Índico", ['site/indico'], ['class' => 'btn-gen btn-indico']) ?>
            <?= Html::a($text = "Mar de China", ['site/china'], ['class' => 'btn-gen btn-china']) ?>
    </div> 
    
        
</div> 


   
