<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\bootstrap4\Button;

/** @var yii\web\View $this */


$this->title = 'Océano índico';
$this->registerJsFile('@web/js/sitejs.js');
?>
<?= Html::a($text = "Mapamundi", ['site/index'], ['class' => 'btn btn-mapamundi btn-dark']) ?>

<div class="containerindico" style="z-index: 1;">    
<h2 class="textind"> Bienvenido al Océano Índico, polizón</h2>

<!-- Menú lateral -->
<p class="textlat"> Selecciona una de las opciones: </p>
      
    <div class="dot"></div>
    
    <?= Html::a('Mapa Índico', ['site/indico'], ['class'=>'btn btn-dark btn-mapassecun'])?>
     <!-- Mapa y botones de piratas -->
    <div class="mapa">
        <?php

        echo Html::img('@web/img/Indico.png', ['alt' => 'Océano Índico']); 
        echo Html::a(Html::img('@web/img/calavera.png', ['alt' => 'Imagen del botón', 'class' => 'imagen-pequena',]),['piratas/williamkidd'], ['class' => ' btn-gen btn-wk', 'title' => 'William Kidd']);
        echo Html::a(Html::img('@web/img/calavera.png', ['alt' => 'Imagen del botón', 'class' => 'imagen-pequena',]),['piratas/olivier'], ['class' => ' btn-gen btn-ol', 'title' => 'Olivier Levasseur']);
       

        ?>
    </div>
 </div>   
<!-- Opción del desplegable para ver las armas -->
     <div class="dot2"></div> 
     <?= Html::a('Armas', ['site/armasindico'], ['class'=>'btn btn-dark btn-datos'])?>
    
         
 




   
