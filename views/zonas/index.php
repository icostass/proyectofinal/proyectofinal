<?php

use app\models\Zonas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Zonas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zonas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Zonas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nombre',
            'longitud',
            'latitud',
            'fecha_dominio',
            'id_pais',
            //'id_zona',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Zonas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_zona' => $model->id_zona]);
                 }
            ],
        ],
    ]); ?>


</div>
