<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Zonas $model */

$this->title = 'Update Zonas: ' . $model->id_zona;
$this->params['breadcrumbs'][] = ['label' => 'Zonas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_zona, 'url' => ['view', 'id_zona' => $model->id_zona]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="zonas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
